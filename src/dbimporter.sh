#!/bin/bash
#DIR = folder form which the function is called
DIR="$(pwd)"
CONFIGFILE="$DIR/.dbconfig"
#Load base config file into VAR=VALUE
source $CONFIGFILE

current_time=$(date "+%Y.%m.%d-%H.%M.%S")

ACTION=$1

if [ -z $ACTION ]
then
	printf "Vous devez specifier une action.\n"
	exit
fi

if [ $ACTION == "init" ]
then
	touch "${DIR}/.dbconfig"
	echo "Fichier de configuration creer. ${DIR}/.dbconfig"
	exit
fi

# Mysql variables instanciation
mmysql=$(which mysql)
mysqld=$(which mysqldump)

if [ -z "$mmysql" ] #if bin does not exists in /usr/local/bin we then fetch via MAMP’s path
then
   mmysql=“/Applications/MAMP/Library/bin/mysql”
   mysqld=“/Applications/MAMP/Library/bin/mysqldump”
fi

function backupStage() {
	current_time=$(date "+%Y.%m.%d-%H.%M.%S")
	outputFileName="dump_$current_time.sql"
	$mysqld --user=$STAGING_DB_USER --password=$STAGING_DB_PWD -h $STAGING_DB_HOST $STAGING_DB_NAME > $DIR/dumps/$outputFileName
	# return $outputFileName
	echo $DIR/dumps/$outputFileName
}

function backupPreprod() {
	current_time=$(date "+%Y.%m.%d-%H.%M.%S")
	outputFileName="dump_$current_time.sql"
	$mysqld --user=$PREPROD_DB_USER --password=$PREPROD_DB_PWD -h $PREPROD_DB_HOST $PREPROD_DB_NAME > $DIR/dumps/$outputFileName
	# return $outputFileName
	echo $DIR/dumps/$outputFileName
}

function backupLocal() {
	current_time=$(date "+%Y.%m.%d-%H.%M.%S")
	outputFileName="dump_$current_time.sql"
	$mysqld --user=$LOCAL_DB_USER --password=$LOCAL_DB_PWD $LOCAL_DB_NAME > $DIR/bkps/$outputFileName
}

function importLocalDump() {
	$mmysql --user=$LOCAL_DB_USER --password=$LOCAL_DB_PWD -e "drop database ${LOCAL_DB_NAME}; create database ${LOCAL_DB_NAME};" #clean up database 
	$mmysqld --user=$LOCAL_DB_USER --password=$LOCAL_DB_PWD $LOCAL_DB_NAME < $1
}

function syncPreStage() {
	$mmysql --user=$STAGING_DB_USER --password=$STAGING_DB_PWD -h $STAGING_DB_HOST -e "drop database ${STAGING_DB_NAME}; create database ${STAGING_DB_NAME};" #clean up database 
	$mmysql --user=$STAGING_DB_USER --password=$STAGING_DB_PWD -h $STAGING_DB_HOST $STAGING_DB_NAME < $1
}

function refreshSitesURL() {
	$mmysql --user=$STAGING_DB_USER --password=$STAGING_DB_PWD -h $STAGING_DB_HOST -D $STAGING_DB_NAME -e "update wp_options set option_value = '$STAGING_HOME_URL' WHERE option_id in (1,2);"
}

if [ $ACTION == "p2l" ]
then
	local_path=$(backupPreprod)
	echo " :: Saved Preprod in $local_path"
	backupLocal
	importLocalDump $local_path
elif [ $ACTION == "p2s" ]; then
	printf "Confirmer que vous voulez envoyer la BD preprod vers la BD stage[Y/n]\n"
	read -e confirm
	if [ $confirm == "y" ] || [ $confirm == "" ]; then
		local_path=$(backupPreprod)
		printf " :: Saved Preprod in ${local_path} ::\n"
		stage_path=$(backupStage)
		printf " :: Saved Stage in $stage_path ::\n"
		syncPreStage $local_path
		refreshSitesURL
	else
		echo 'Okay debord :('
	fi
fi

