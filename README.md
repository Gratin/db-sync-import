# Installation

Clonez le projet, allez dans le dossier et lancer `sh install.sh`

# Utilisation

Lorsque vous voulez utiliser cet outil pour un projet, dirigez vous dans le dossier du projet et faites `dbimporter init`. Cette fonction ne fait que creer un fichier `.dbconfig` a la racine du dossier.

## Pour synchroniser vers votre environement local

Vous pouvez invoquer l'app ainsi `dbimporter p2l` (preprod to local). Cette option creer un backup de l'environnement preprod sur votre ordinateur local installera la BD sur votre environnement.

## Pour synchroniser de la pre-prod vers le staging


Vous pouvez invoquer l'app ainsi `dbimporter p2s` (preprod to staging). Cette option creer un backup de l'environnement preprod sur votre ordinateur local, ainsi que le staging. Ensuite il supprimera la BD du stage pour y importer celle de la preprod (le programme se chargera de faire le changement d'url si l'url est specifié dans les configs)


## Configuration

PREPROD_DB_USER=. 

PREPROD_DB_NAME=. 

PREPROD_DB_PWD=. 

PREPROD_DB_HOST=. 


STAGING_DB_USER=. 

STAGING_DB_NAME=. 

STAGING_DB_PWD=. 

STAGING_DB_HOST=. 


LOCAL_DB_USER=. 

LOCAL_DB_PWD=. 

LOCAL_DB_NAME=. 

LOCAL_DB_HOST=. 



STAGING_HOME_URL="". 

